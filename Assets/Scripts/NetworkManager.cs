using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;
using Photon.Realtime;
using UnityEngine.Serialization;
using UnityEngine.UI;

public class NetworkManager : MonoBehaviourPunCallbacks
{
    [Header("Connection Status Panel")] 
    public Text connectionStatus_TXT;
    
    [Header("Login UI Panel")] 
    public InputField playerName_Input;
    public GameObject loginUI_Panel;

    [Header("Game Options Panel")]
    public GameObject gameOption_Panel;

    [Header("Create Room Panel")] 
    public GameObject CreateRoom_Panel;
    public InputField roomName_Input;
    public InputField playerCount_Input;

    [Header("Join Random Room Panel")] 
    public GameObject JoinRandomRoom_Panel;
    
    [Header("Show Room Panel")] 
    public GameObject ShowRoomList_Panel;
    
    [Header("Inside Room Panel")] 
    public GameObject InsideRoom_Panel;
    public Text roomInfo_TXT;
    public GameObject playerlistItem_prefab;
    public Transform playerListViewParent;
    public GameObject startGameBTN;
    
    [Header("Room List Panel")] 
    public GameObject roomList_panel;
    public GameObject roomItem_Prefab;
    public GameObject roomListParent;

    private Dictionary<string, RoomInfo> cachedRoomList;
    private Dictionary<string, GameObject> roomListGameObjects;
    private Dictionary<int, GameObject> playerListGameObjects;

    #region Unity Functions

    void Start()
    {
        cachedRoomList = new Dictionary<string, RoomInfo>();
        roomListGameObjects = new Dictionary<string, GameObject>();
        ActivatePanel(loginUI_Panel);

        PhotonNetwork.AutomaticallySyncScene = true;
        
    }
    
    void Update()
    {
        connectionStatus_TXT.text = $"Connection status: {PhotonNetwork.NetworkClientState}";
    }

    #endregion

    #region UI Callbacks

    public void OnLoginButtonClicked()
    {
        string playerName = playerName_Input.text;

        if (string.IsNullOrEmpty(playerName))
        {
            Debug.Log("player name is empty!");
            return;
        }

        PhotonNetwork.LocalPlayer.NickName = playerName;
        PhotonNetwork.ConnectUsingSettings();
    }

    public void OnCreateRoomBtnClicked()
    {
        string roomName = roomName_Input.text;

        if (string.IsNullOrEmpty(roomName))
        {
            roomName = $"Room {Random.Range(1000, 10000)}";
        }

        RoomOptions roomOptions = new RoomOptions();
        roomOptions.MaxPlayers = (byte)int.Parse(playerCount_Input.text);
        PhotonNetwork.CreateRoom(roomName, roomOptions);
    }

    public void OnCancelBtnClicked()
    {
        ActivatePanel(gameOption_Panel);
    }

    public void OnShowRoomBtnClicked()
    {
        if (!PhotonNetwork.InLobby) PhotonNetwork.JoinLobby();
        
        ActivatePanel(ShowRoomList_Panel);
    }

    public void OnBackBtnClick()
    {
        if (PhotonNetwork.InLobby)
        {
             
            PhotonNetwork.LeaveLobby();
        }
        
        ActivatePanel(gameOption_Panel);
    }
    
    public void OnLeaveBtnClick() { PhotonNetwork.LeaveRoom(); }

    public void OnRandomBtnClick()
    {
        ActivatePanel(JoinRandomRoom_Panel);
        PhotonNetwork.JoinRandomRoom();
        
    }

    public void OnStartGameBtnClick()
    {
        PhotonNetwork.LoadLevel("GameScene");
    }
    
    #endregion

    #region PUN Callbacks

    public override void OnConnected()
    {
        Debug.Log("Connected to the internet");
    }

    public override void OnConnectedToMaster()
    {
        Debug.Log($"{PhotonNetwork.LocalPlayer.NickName} has connected to Photon Servers.");
        ActivatePanel(gameOption_Panel);
    }

    public override void OnCreatedRoom()
    {
        Debug.Log($"{PhotonNetwork.CurrentRoom.Name} Created!");
    }

    public override void OnJoinedRoom()
    {
        startGameBTN.SetActive(PhotonNetwork.LocalPlayer.IsMasterClient);
        Debug.Log($"{PhotonNetwork.LocalPlayer.NickName} has joined {PhotonNetwork.CurrentRoom.Name}");
        ActivatePanel(InsideRoom_Panel);
        roomInfo_TXT.text = $"Room Name: {PhotonNetwork.CurrentRoom.Name} " +
                            $"Current Player Count: {PhotonNetwork.CurrentRoom.PlayerCount}/{PhotonNetwork.CurrentRoom.MaxPlayers}";

        if (playerListGameObjects == null)
        {
            playerListGameObjects = new Dictionary<int, GameObject>();
        }

        foreach (Player player in PhotonNetwork.PlayerList)
        {
            GameObject playerItem = Instantiate(playerlistItem_prefab, playerListViewParent);
            playerItem.transform.localScale = Vector3.one;

            playerItem.transform.Find("PlayerNameText").GetComponent<Text>().text = player.NickName;

            playerItem.transform.Find("PlayerIndicator").gameObject
                .SetActive(player.ActorNumber == PhotonNetwork.LocalPlayer.ActorNumber);
            
            playerListGameObjects.Add(player.ActorNumber, playerItem);
        }
    }

    // Other Player Joined Room
    public override void OnPlayerEnteredRoom(Player player)
    {
        startGameBTN.SetActive(PhotonNetwork.LocalPlayer.IsMasterClient);
        roomInfo_TXT.text = $"Room Name: {PhotonNetwork.CurrentRoom.Name} " +
                            $"Current Player Count: {PhotonNetwork.CurrentRoom.PlayerCount}/{PhotonNetwork.CurrentRoom.MaxPlayers}";
        
        GameObject playerItem = Instantiate(playerlistItem_prefab, playerListViewParent);
        playerItem.transform.localScale = Vector3.one;

        playerItem.transform.Find("PlayerNameText").GetComponent<Text>().text = player.NickName;

        playerItem.transform.Find("PlayerIndicator").gameObject
            .SetActive(player.ActorNumber == PhotonNetwork.LocalPlayer.ActorNumber);
            
        playerListGameObjects.Add(player.ActorNumber, playerItem);
    }

    //other player leaves
    public override void OnPlayerLeftRoom(Player otherPlayer)
    {
        startGameBTN.SetActive(PhotonNetwork.LocalPlayer.IsMasterClient);
        roomInfo_TXT.text = $"Room Name: {PhotonNetwork.CurrentRoom.Name} " +
                            $"Current Player Count: {PhotonNetwork.CurrentRoom.PlayerCount}/{PhotonNetwork.CurrentRoom.MaxPlayers}";
        
        Destroy(playerListGameObjects[otherPlayer.ActorNumber]);
        playerListGameObjects.Remove(otherPlayer.ActorNumber);
    }

    // Local Player(YOU) leave
    public override void OnLeftRoom()
    {
        foreach (var obj in playerListGameObjects.Values)
        {
            Destroy(obj);
        }
        
        playerListGameObjects.Clear();
        playerListGameObjects = null;
        
        ActivatePanel(gameOption_Panel);
    }
    public override void OnRoomListUpdate(List<RoomInfo> roomList)
    {
        ClearRoomGameObjects();
        
        startGameBTN.SetActive(PhotonNetwork.LocalPlayer.IsMasterClient);
        
        foreach (RoomInfo info in roomList)
        {
            Debug.Log(info.Name);
            if (!info.IsOpen || !info.IsVisible || info.RemovedFromList)
            {
                if (cachedRoomList.ContainsKey(info.Name))
                {
                    cachedRoomList.Remove(info.Name);
                }
            }
            else
            {
                if (cachedRoomList.ContainsKey(info.Name))
                {
                    cachedRoomList[info.Name] = info;
                }
                else
                {
                    cachedRoomList.Add(info.Name, info);
                }
            }
        }

        foreach (RoomInfo info in cachedRoomList.Values)
        {
            Debug.Log("hhhhh: " + info.Name);
            GameObject listItem = Instantiate(roomItem_Prefab, roomListParent.transform);
            //listItem.transform.SetParent(roomListParent.transform);
            listItem.transform.localScale = Vector3.one;

            listItem.transform.Find("RoomNameText").GetComponent<Text>().text = info.Name;
            listItem.transform.Find("RoomPlayersText").GetComponent<Text>().text =
                $"Player Count: {info.PlayerCount}/{info.MaxPlayers}";
            listItem.transform.Find("JoinRoomButton").GetComponent<Button>().onClick.AddListener(() => OnJoinRoomClick(info.Name));

            roomListGameObjects.Add(info.Name, listItem);
        }
    }

    public override void OnLeftLobby()
    {
        ClearRoomGameObjects();
        cachedRoomList.Clear();
    }

    public override void OnJoinRandomFailed(short returnCode, string message)
    {
        Debug.LogWarning(message);
        string roomName = $"Room {Random.Range(1000, 10000)}";
        RoomOptions roomOptions = new RoomOptions();
        roomOptions.MaxPlayers = 20;

        PhotonNetwork.CreateRoom(roomName, roomOptions);
    }
    
    #endregion

    #region Public Methods

    public void ActivatePanel(GameObject panelToActivated)
    {
        loginUI_Panel.SetActive(panelToActivated.Equals(loginUI_Panel));
        gameOption_Panel.SetActive(panelToActivated.Equals(gameOption_Panel));
        CreateRoom_Panel.SetActive(panelToActivated.Equals(CreateRoom_Panel));
        JoinRandomRoom_Panel.SetActive(panelToActivated.Equals(JoinRandomRoom_Panel));
        ShowRoomList_Panel.SetActive(panelToActivated.Equals(ShowRoomList_Panel));
        InsideRoom_Panel.SetActive(panelToActivated.Equals(InsideRoom_Panel));
        roomList_panel.SetActive(panelToActivated.Equals(roomList_panel));
    }

    #endregion

    #region PrivateMethods

    void OnJoinRoomClick(string roomName)
    {
        if (PhotonNetwork.InLobby) PhotonNetwork.LeaveLobby();
        
        PhotonNetwork.JoinRoom(roomName);
    }

    private void ClearRoomGameObjects()
    {
        foreach (var item in roomListGameObjects.Values)
        {
            Destroy(item);
        }
        roomListGameObjects.Clear();
    }

    #endregion
}
